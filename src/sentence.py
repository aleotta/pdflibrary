
#Part-of-speech (POS) tag
#Classification
#Parsing, Chunking, & Grammar Writing
#Propositional Semantics & Logic

#from nltk.book import *

import nltk
import os
import sys
import csv

datapath="F:\\pdfs_meta"

did = "35a10971-70c0-43ec-8336-8ae79fe06ba5"

filename = os.path.join(datapath, did +" info.csv")
file = open(filename, "rb")
info = file.read()
file.close()

print info


filename = os.path.join(datapath, did +".txt")
print("filename=%s"%(filename))

file = open(filename, "rb")
raw = file.read()
file.close()

#text = nltk.data.load(filename, format="raw")
#print("length = %d"%(len(text)))

sentences = nltk.tokenize.sent_tokenize(raw)

#sys.exit()
filename = os.path.join(datapath, did +" sentences.csv")
file = open(filename, "wb")
cwvwriter=csv.writer(file,delimiter=",")
header=[]
header.append("index")
header.append("txt")
cwvwriter.writerow(header)
index=0
for sentence in sentences:
	index+=1
	#print index,sentence[0:10]
	#file.write(sentence)	
	#file.write("\n")	
	
	row=[]
	row.append(index)
	row.append(sentence)
	cwvwriter.writerow(row)
	
file.close()