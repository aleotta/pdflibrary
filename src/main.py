'''
Created on May 5, 2014

@author: Tony
'''

#http://www.lfd.uci.edu/~gohlke/pythonlibs/

import os
import sys
import string
import csv
import operator
import re
import datetime
from time import gmtime, strftime
import tempfile 
import uuid
from md5 import md5
import shutil
    
class FrequentWord():
    def __init__(self, filename):
        self.words={}
        self.filename = filename
        self.load()
        
    def __del__(self):
        self.save()
        
    def load(self):
        if not os.path.exists(self.filename):            
            self.save()
            
        fh = open(self.filename, "rb")
        
        csvreader = csv.reader(fh,delimiter="\t")
        csvreader.next()
        idx=1
        for row in csvreader:
            if len(row)>0:
                word = row[0].strip()
                if len(word)>0:   
                    #print line
                    self.words[word]=idx
                    idx+=1
    
        fh.close()
    
    def save(self):
        
        fh = open(self.filename, "wb")
        
        csvwriter = csv.writer(fh,delimiter="\t")
    
        columnNames=[]
        columnNames.append('Word')
        csvwriter.writerow(columnNames)
        
        
        #keys = sorted(self.words.values())
        keys = sorted(self.words, key=self.words.get)
        #print keys
        idx=1
        for word in keys:
            data=[]
            data.append(word)        
            csvwriter.writerow(data)
        
        fh.close() 
    
    def add(self, word):
        max=len(self.words)
        
        if word not in self.words:            
            self.words[word]=max+1
    
    def exists(self, word, limit=-1):
        if word not in self.words:
            return False
        else:            
            if limit==-1:
                return True
            idx = self.words[word]
            
            if idx<limit:
                return False
            else:
                return True


class StopWord(FrequentWord):
    
    def save(self):
        
        fh = open(self.filename, "wb")
        
        csvwriter = csv.writer(fh,delimiter="\t")
    
        columnNames=[]
        columnNames.append('Word')
        csvwriter.writerow(columnNames)
        
        keys = sorted(self.words.keys())
        
        for word in keys:
            data=[]
            data.append(word)        
            csvwriter.writerow(data)
        
        fh.close() 
    

            
    
    
    
  

# Run the above function and store its results in a variable.   

def extract_metadata(filename, target_path, random_filename):
    
    (shortname, extension) = os.path.splitext(filename)
    shortname2 = os.path.basename(filename)
    path = filename[:len(filename)-len(os.path.basename(filename))]
    #output_filename = "%s%s_meta.txt"%(path,os.path.basename(shortname))
    #output_filename = os.path.join(target_path,os.path.basename(shortname)+" meta.csv")
    
    output_filename = os.path.join(target_path,random_filename+" meta.csv")
    
    
    if not os.path.exists(output_filename):
        exe = "pdfinfo.exe \"%s\" > \"%s\""%(filename,output_filename)
        #print exe
        os.system(exe)
            
    #outDataFilename = os.path.join(target_path, os.path.basename(shortname) + " info.csv")
    outDataFilename = os.path.join(target_path, random_filename + " info.csv")
    
    try:
        outDataFile = open(outDataFilename, "wb")
        csvwriter = csv.writer(outDataFile,delimiter=",")
        
        line=[]
        line.append("filename")
        line.append("path")
        csvwriter.writerow(line)
        
        line=[]
        line.append(shortname2)
        line.append(path)
        csvwriter.writerow(line)        
         
        outDataFile.close()
        
    except:
        pass
    
    if os.path.exists(output_filename):
        return True
    else:
        return False
    
def extract_images(filename):
    
    (shortname, extension) = os.path.splitext(filename)
        
    try:
        os.mkdir(os.path.basename(shortname))
    except:
        pass
     
    os.chdir(os.path.basename(shortname))
    exe = "pdfimages.exe -j \"%s\" \"%s\""%(filename, shortname)
    print exe
    os.system(exe)            
    os.chdir("..")

def extract_text(filename,target_path,random_filename):
    (shortname, extension) = os.path.splitext(filename)
    shortname2 = os.path.basename(filename)
    path = filename[:len(filename)-len(os.path.basename(filename))]    
    #output_filename = os.path.join(target_path,os.path.basename(shortname)+".txt")
    output_filename = os.path.join(target_path, random_filename +".txt")

    if not os.path.exists(output_filename):
        exe = "pdftotext.exe \"%s\" \"%s\""%(filename,output_filename)
        #print exe
        os.system(exe)

    if os.path.exists(output_filename):
        return True
    else:
        return False

def compareItems((w1,c1), (w2,c2)):
    if c1 > c2:
        return - 1
    elif c1 == c2:
        return cmp(w1, w2)
    else:
        return 1
                        
def is_boolean(s):
        
    if (s=='true') or (s=='false'): 
        return True

    if (s=='yes') or (s=='no') or (s=='not-sure'): 
        return True

    return False

def is_float(s):
    match = re.match('^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$', s)
    if match:
        return True
    else:
        return False

def is_integer(s):
    match = re.match('^[0-9]+$', s)
    if match:
        return True
    else:
        return False

def is_number(s):
           
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False

    return True

def is_date(s):

    match = re.match('^[0-9]+/[0-9]+/[0-9]+$', s)
    if match:
        return True
    
    match = re.match('^[0-9]{4}-[0-9]{2}$', s)
    if match:
        return True

    #match = re.match('[0-9]{1}:[0-9]{2}', s)
    #if match:
    #    return True

    match = re.match('^([0-9]+)(:|-)([0-9]+)$', s)
    if match:
        return True

    match = re.match('^[0-9]{2}:[0-9]{2}:[0-9]{2}$', s)
    if match:
        return True

    return False

    
                                
def gen_word_counts(freqWords, ignoreWords, stopWords, filename, target_path,random_filename):   
    
    #print "========= %s ============="%(filename)
    #n=10
    (shortname, extension) = os.path.splitext(filename)
    shortname2 = os.path.basename(filename)
    path = filename[:len(filename)-len(os.path.basename(filename))]

    #output_filename = os.path.join(target_path,os.path.basename(shortname)+" freq.csv")    
    output_filename = os.path.join(target_path,random_filename+" freq.csv")
    #input_filename = os.path.join(target_path,os.path.basename(shortname)+".txt")
    input_filename = os.path.join(target_path,random_filename+".txt")
    
    if not os.path.exists(input_filename):
        print "Error %s not found"%(input_filename)
        return
        
    if not os.path.exists(output_filename):
        f = open(input_filename)
        output = open(output_filename, "wb")
        
        csvwriter = csv.writer(output,delimiter="\t")
    
        columnNames=[]
        columnNames.append('Word')
        columnNames.append('Count')
        csvwriter.writerow(columnNames)
        
                
        text = open(input_filename,'r').read()
        text = string.lower(text)
        for ch in '!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~':
            text = string.replace(text, ch, ' ')
            words = string.split(text)
         
            words_filtered=[]
            for word in words:
                if len(word)<3:
                    continue
                
                if is_number(word):
                    continue
                
                if stopWords.exists(word):
                    continue

                words_filtered.append(word)

                                         
#             if freqWords.exists(word):
#                 continue
#             if ignoreWords.exists(word):
#                 continue
#             if is_date(word):
#                 continue
#             #else:
#             #    print "filtered common %s"%(word)
                    
        # construct a dictionary of word counts
        counts = {}
        for w in words_filtered:
            counts[w] = counts.get(w,0) + 1
        
        n = len(counts)
        items = counts.items()
        items.sort(compareItems)
        
        for i in range(n):
            #print "%s\t%d" % items[i] 
            #print type(items[i][1])        
            data=[]
            data.append(items[i][0])
            data.append(items[i][1])
            csvwriter.writerow(data)

        output.close()
        return items

def gen_checksum(filename):
    try:
        with open( filename ) as openfile:
            filehash = md5( openfile.read() ).hexdigest()
        return filehash
    except:
        return -1



# copies a file to a new unique filename
def backup_file(filename):
    if os.path.exists(filename):
        i=1
        while i > -1:
            backup_filename = filename+";"+str(i)
            if not os.path.exists(backup_filename):
                shutil.copy(filename, backup_filename)
                return
            i+=1


class FileSystemModel:
    
    def __init__(self):
        self.oldfilelist = {}
        self.newfilelist = []
        self.directory = ""
    
    def files(self):
        return self.newfilelist
    
    def did_exist(self, filename):
        pass
    
    def exists(self, filename):
        pass
    
    def load(self):
        self.load_file_list()
  
    def save(self):
        self.save_file_list()
  
    def get_files(self,directory):
        """
        This function will generate the file names in a directory 
        tree by walking the tree either top-down or bottom-up. For each 
        directory in the tree rooted at directory top (including top itself), 
        it yields a 3-tuple (dirpath, dirnames, filenames).
        """
        self.directory=directory
        self.newfilelist = []  # List which will store all of the full filepaths.
    
        # Walk the tree.
        for root, directories, files in os.walk(directory):
            for filename in files:
                # Join the two strings in order to form the full filepath.
                filepath = os.path.join(root, filename)
                self.newfilelist.append(filepath)  # Add it to the list.
    
    # saves self.newfilelist to a csv
    def save_file_list(self):
        filename="files.csv"
        backup_file(filename)    
                
        file = open(filename,"wb")
        csvwriter = csv.writer(file, delimiter=",")
        header=[]
        header.append("file")        
        csvwriter.writerow(header)
        
        for filename in self.newfilelist:
            data=[]
            data.append(filename)
            csvwriter.writerow(data)
    
        file.close()
    
    # loads self.oldfilelist from a csv
    def load_file_list(self):        
        filename="files.csv"
        file = open(filename,"rb")
        csvreader = csv.reader(file, delimiter=",")
        header = csvreader.next()
        print(header)
        for row in csvreader:        
            self.oldfilelist[row[0]]=row        
        file.close()

class Library():
    def __init__(self):
        self.db={}
        self.dbfilename="log.txt"
        self.file_index=0
        self.file_success_index=0
        self.file_failed_index=0
        self.fs = FileSystemModel()
        
    def size(self):
        return len(self.db())
    
    def open(self):
        backup_file(self.dbfilename)        
        self.load_dbfile()        
    
        self.logFile = open(self.dbfilename,"wb")
        self.csvwriter = csv.writer(self.logFile, delimiter=",")
        header=[]
        header.append("time")
        header.append("file")
        header.append("alias")
        header.append("size")
        header.append("checksum")
        header.append("status")
            
        self.csvwriter.writerow(header)
        self.file_index=0
        self.file_success_index=0
        self.file_failed_index=0

    def scan(self, path, target_path):
                
        self.fs = FileSystemModel()
        self.fs.load()
        self.fs.get_files(path)
        self.fs.save()
            
        for filename in self.fs.files():
            (shortname, extension) = os.path.splitext(filename)
                            
            if extension==".pdf":
                if filename not in self.db:
                    if os.path.exists(filename):
                        self.file_index+=1
                        checksum = gen_checksum(filename)
                        filesize = os.path.getsize(filename)
                        
                        row=[]
                        row.append(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
                        row.append(filename)
                                    
                        random_filename = str(uuid.uuid4())
                        row.append(random_filename)            
                        row.append(filesize)
                        row.append(checksum)
                                                                        
                        success=False
                        success = extract_metadata(filename, target_path, random_filename)
                        if success:    
                            #extract_images(filename)
                            success = extract_text(filename, target_path,random_filename)
                            if success:
                                self.file_success_index+=1
                                print("%6d %6d %4d %s"%(self.file_index, self.file_success_index, self.file_failed_index, filename))
                                # put this on hold
                                #items = gen_word_counts(freqWords, ignoreWords, stopWords, filename, target_path, random_filename)
            
                        if success:
                            row.append('1')                    
                        else:
                            row.append('0')
                            self.file_failed_index+=1
                            
                        self.logcsv.writerow(row)
                    
                else:                
                    row = self.db[filename]
                    print("skipping %s"%(row))                
                    self.logcsv.writerow(row)
                
                self.logFile.flush()
                    
        self.logFile.close()
    
    
    def close(self):
        self.logFile.close()
               
    def load_dbfile(self):
        logFile = open(self.dbfilename,"rb")
        logcsv = csv.reader(logFile, delimiter=",")
        self.filedb={}
        next(logcsv, None)  # skip the headers
        for row in logcsv:
            self.filedb[row[1]]=row            
        logFile.close()        
        
def main():
    path = "F:\\pdfs"
    target_path = "F:\\pdfs_meta"
            
    library = Library()
    library.load()    
    print("library contains %d file entries"%(library.sized()))    
    library.scan(path,target_path)
    
    #stopWords = StopWord("stop.csv")    
    #freqWords = FrequentWord("freq.csv")
    #ignoreWords = FrequentWord("ignore.csv")    
    #stopWords.save()
    #freqWords.save()
     
if __name__ == '__main__':
    main()
    
    
    
    